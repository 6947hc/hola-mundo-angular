import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";


@Injectable({
    providedIn: 'root'
})

export class DataService {
    nombre: string = "sin nombre";
    direccion: string = '';


    private _dataSource = new BehaviorSubject<string>('');
    // asObervable convierte a un oservable
    //datasourse$ es una variable
// en el this tiene la reaccion a otra variable
    dataSource$ = this._dataSource.asObservable();
    constructor()  {}

    public ModificarDireccion(_direccion : string): void{
        this.direccion = _direccion;

        this._dataSource.next(this.direccion);
    }
}