import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {


  parentMessage = 'Mensaje del padre';
  mensajePadre!: string;


  constructor() { }

  ngOnInit(): void {
  }
  recibirMensaje($event: string){
    this.mensajePadre = $event;
    console.log(this.mensajePadre);
  }



}
