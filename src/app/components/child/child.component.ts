import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {


  mensaje = 'mensaje del hijo al padre';
  @Input() ChildMessage!: string;
  @Output() eventoMensaje = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
    console.log(this.ChildMessage);
    this.eventoMensaje.emit(this.ChildMessage);
    
  }
}
