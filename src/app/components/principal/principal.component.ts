import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/servicio/data.service';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {

   dir! : string;

   constructor(dataService: DataService) {
    dataService.dataSource$.subscribe((data: string) => {
      console.log(data);
      this.dir = data;
    });
  }

  ngOnInit(): void {
  }

}
